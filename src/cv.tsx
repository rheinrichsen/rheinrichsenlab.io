import './roboto.css'
import styles from './cv.module.css'
import { ProjectList } from "./components/projects/project-list.tsx";
import { EducationList } from "./components/education/education-list.tsx";
import { JobList } from "./components/jobs/job-list.tsx";
import { Personal } from "./components/personal/personal.tsx";
import { Footer } from './components/footer/footer.tsx';
import {DownloadButton} from "./components/download-button/download-button.tsx";

export const Cv: React.FC = () => {
    return (
        <>
            <DownloadButton/>
            <div id="cv" className={styles.cv}>
                <div className={styles.personal}>
                    <Personal/>
                </div>
                <div className={styles.experience}>
                    <JobList/>
                    <EducationList/>
                    <ProjectList/>
                </div>
            </div>
            <Footer/>
        </>
    )
}

