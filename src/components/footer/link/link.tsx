import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './link.module.css'
import { LinkData } from "../data/links.ts";
import React from "react";

interface LinkProps {
    linkData: LinkData
}

export const Link: React.FC<LinkProps> = ({linkData}) => {
    return (
        <a className={styles.link} href={linkData.link} target="_blank">
            <FontAwesomeIcon className={styles.icon} color="#FFFFFF" icon={linkData.icon}/>
        </a>
    )
}

