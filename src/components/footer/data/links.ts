import {faGitlab, faLinkedin, faXing, faXTwitter, IconDefinition} from "@fortawesome/free-brands-svg-icons";

export type LinkData = {
    link: string
    icon: IconDefinition
}

export const links: Array<LinkData> = [
    {
        link: 'https://gitlab.com/rheinrichsen',
        icon: faGitlab
    },
    {
        link: 'https://www.xing.com/profile/Robert_Heinrichsen',
        icon: faXing
    },
    {
        link: 'https://twitter.com/robheinrichsen',
        icon: faXTwitter
    },
    {
        link: 'https://www.linkedin.com/in/robert-heinrichsen-728890263/',
        icon: faLinkedin
    }
]