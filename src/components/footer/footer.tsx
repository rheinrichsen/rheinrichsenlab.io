import styles from './footer.module.css'
import React from "react";
import { Link } from "./link/link.tsx";
import { links } from "./data/links.ts";

export const Footer: React.FC = ({}) => {
    return (
        <footer className={styles.footer}>
            <div className={styles.text}>Find me online</div>
            {links.map((link, index) => (
                <Link key={index} linkData={link}/>
            ))}
            <div className={styles.text}>By Robert Heinrichsen</div>
        </footer>
    )
}

