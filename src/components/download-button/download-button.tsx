import styles from './download-button.module.css'

export const DownloadButton: React.FC = ({}) => {

    return (
        <div className={styles.downloadSection}>
            <a className={styles.downloadButton} href={"cv_robert_heinrichsen.pdf"} target={"_blank"}>
                Download as PDF
            </a>
        </div>
    )
}

