import React from 'react';
import styles from './title.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconDefinition } from "@fortawesome/free-brands-svg-icons";

export enum TitleSize {
    MEDIUM = 'MEDIUM',
    LARGE = 'LARGE'
}

interface TitleProps {
    icon: IconDefinition
    title: string
    size?: TitleSize
}

export const Title: React.FC<TitleProps> = ({icon, title, size = TitleSize.LARGE}) => {
    const titleSizeClass = (): string => {
        switch(size) {
            case TitleSize.MEDIUM: return styles.medium
            case TitleSize.LARGE: return styles.large
        }
    }


    return (
        <div className={`${styles.title} ${titleSizeClass()}`}>
            <FontAwesomeIcon className={`${styles.titleIcon} ${titleSizeClass()}`} icon={icon}/>
            <span>{title}</span>
        </div>
    )
}



