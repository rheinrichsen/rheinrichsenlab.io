import React from 'react';
import styles from './personal-title.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconDefinition } from "@fortawesome/free-brands-svg-icons";

interface TitleProps {
    icon: IconDefinition
    title: string
}

export const PersonalTitle: React.FC<TitleProps> = ({icon, title}) => {
    return (
        <div className={styles.titleTest}>
            <FontAwesomeIcon className={styles.titleIcon} icon={icon}/>
            <span>{title}</span>
        </div>
    )
}



