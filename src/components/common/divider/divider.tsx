import React from 'react';
import styles from './divider.module.css'

export enum DividerSize {
    SMALL = 'SMALL',
    MEDIUM = 'MEDIUM',
    LARGE = 'LARGE'
}

type DividerProps = {
    size?: DividerSize
}

export const Divider: React.FC<DividerProps> = ({size = DividerSize.SMALL}) => {
    const dividerSizeClass = (): string => {
        switch(size) {
            case DividerSize.SMALL: return styles.small
            case DividerSize.MEDIUM: return styles.medium
            case DividerSize.LARGE: return styles.large
        }
    }

    return (
        <hr className={`${styles.divider} ${dividerSizeClass()}`}/>
    )
}



