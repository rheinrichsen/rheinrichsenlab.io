import React from 'react';
import { JobEntry } from "./job-entry.tsx";
import { jobs } from './data/jobs.ts'
import { faSuitcase } from "@fortawesome/free-solid-svg-icons";
import styles from './job-list.module.css'
import {Title} from "../common/title/title.tsx";
import {Divider} from "../common/divider/divider.tsx";

export const JobList: React.FC = ({}) => {
    return (
        <div className={styles.jobList}>
            <Title title={"Work Experience"} icon={faSuitcase}/>
            {jobs.map((job, index) => (
                <React.Fragment key={index}>
                    <JobEntry jobData={job}/>
                    {index !== jobs.length - 1 && <Divider/>}
                </React.Fragment>
            ))}
        </div>
    )
}



