import React from 'react';
import { JobData } from "./data/jobs.ts";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendar } from "@fortawesome/free-solid-svg-icons";
import styles from './job-entry.module.css'

interface JobProps {
    jobData: JobData
}

export const JobEntry: React.FC<JobProps> = ({jobData}) => {

    const renderCompany = (company?: string): string => {
        return company ? ` / ${company}` : '';
    }

    const renderTasksHeadline = (to?: string) => {
        if (to) {
            return 'Tasks:'
        } else {
            return 'Current Daily Business:'
        }
    }

    return (
        <div className={styles.entry}>
            <div className={styles.headline}>{jobData.title} {renderCompany(jobData.company)}</div>
            <div className={styles.date}>
                <FontAwesomeIcon className={styles.dateIcon} icon={faCalendar}/>
                <span>{jobData.from}</span> - {jobData.to ? (<span>{jobData.to}</span>) : (<span className={styles.dateCurrent}>Current</span>)}
            </div>
            <div className={styles.tasks}>
                <div className={styles.tasksHeadline}>{renderTasksHeadline(jobData.to)}</div>
                <ul>
                    {jobData.tasks.map((task, index) => (
                        <li className={styles.listEntry} key={index}>{task}</li>
                    ))}
                </ul>
            </div>
            {jobData.technologies ? (
                <div className={styles.technologies}>
                    <div className={styles.technologiesHeadline}>Technologies:</div>
                    <ul>
                        {jobData.technologies.map((technologie, index) => (
                            <li className={styles.listEntry} key={index}>{technologie}</li>
                        ))}
                    </ul>
                </div>
            ) : null}
        </div>
    )
}