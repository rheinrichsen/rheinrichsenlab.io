export type JobData = {
    company?: string
    description?: string
    from: string
    tasks: string[]
    technologies?: string[]
    title: string
    to?: string
}

export const jobs: Array<JobData> = [
    {
        company: 'Engel & Völkers Technology GmbH',
        from: '02/2023',
        tasks: [
            'Work as a fullstack engineer on a financial application',
            'Part of an agile team'
        ],
        technologies: [
            'node.js Backend with Typescript',
            'React Frontend with Typescript'
        ],
        title: 'Senior Software Engineer'
    },
    {
        company: 'diva-e next GmbH',
        from: '09/2013',
        tasks: [
            'Planned and support IT-projects from the first concepts until after-release platform support',
            'Analyzed functional and technical requirements',
            'Designed CMS architectures in context of scaling platforms',
            'Consulted and supported customers at design approaches and delivery of their projects',
            'Worked as "Full-Stack"-developer in agile teams',
            'Supported in frontend development with Vue/Nuxt (Javascript/Typescript)',
            'Supported in backend development with Spring (Java/Kotlin)',
            'Supported in maintenance/scripting/other topics with anything thats well suited for the job'
        ],
        title: 'Software Developer - CMS Expert and Web-Applications developer',
        to: '01/2023',
    },
    {
        company: '02/2011',
        from: '05/2006',
        tasks: [
            'Created and designed different small homepages'
        ],
        title: 'Freelance Homepage-/Webdesigner',
        to: '01/2010'
    }
]
