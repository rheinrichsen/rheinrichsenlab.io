import React from 'react';

import styles from "./entry.module.css";
import {CodingLanguageData} from "../data/coding-languages.ts";

interface EntryProps {
    codingLanguageData: CodingLanguageData
}

export const Entry: React.FC<EntryProps> = ({codingLanguageData}) => {

    const codingLanguageWidth = {
        width: `${codingLanguageData.width}%`
    };

    return (
        <div className={styles.entry}>
            <div>{codingLanguageData.name}:</div>
            <div className={styles.contentBarOuter} style={codingLanguageWidth}>
                <div className={styles.contentBarInner}>
                    {codingLanguageData.experience}
                </div>
            </div>
        </div>
    )
}