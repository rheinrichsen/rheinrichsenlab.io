import React from 'react';

import { faCode } from "@fortawesome/free-solid-svg-icons";
import { codingLanguages } from "./data/coding-languages.ts";
import { Entry } from "./entry/entry.tsx";
import styles from './section.module.css'
import {Title, TitleSize} from "../../common/title/title.tsx";

export const Section: React.FC = ({}) => {
    return (
        <div className={styles.list}>
            <Title title={"Coding Languages"} icon={faCode} size={TitleSize.MEDIUM}/>
            {codingLanguages.map((codingLanguage, index) => (
                <React.Fragment key={index}>
                    <Entry codingLanguageData={codingLanguage}/>
                </React.Fragment>
            ))}
        </div>
    )
}