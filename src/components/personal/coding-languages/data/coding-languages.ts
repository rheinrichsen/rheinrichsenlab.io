
export type CodingLanguageData = {
    name: string
    experience: string
    width: number
}

export const codingLanguages: Array<CodingLanguageData> = [
    {
        name: 'Java / Kotlin',
        experience: '5 years',
        width: 100
    },
    {
        name: 'Javascript / Typescript',
        experience: '4 years',
        width: 80
    },
    {
        name: 'Python',
        experience: '2 years',
        width: 40
    }
]