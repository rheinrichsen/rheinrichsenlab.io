import React from 'react';

import {faAsterisk} from "@fortawesome/free-solid-svg-icons";
import { skills } from './data/skills.ts'
import styles from './section.module.css'
import { Entry } from "./entry/entry.tsx";
import {Title, TitleSize} from "../../common/title/title.tsx";
export const Section: React.FC = ({}) => {
    return (
        <div className={styles.list}>
            <Title title={"Skills"} icon={faAsterisk} size={TitleSize.MEDIUM}/>
            {skills.map((skill, index) => (
                <React.Fragment key={index}>
                    <Entry skillData={skill}/>
                </React.Fragment>
            ))}
        </div>
    )
}