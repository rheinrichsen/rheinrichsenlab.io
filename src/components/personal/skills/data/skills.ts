import {IconDefinition} from "@fortawesome/free-brands-svg-icons";
import {
    faAlignJustify,
    faBugs, faCloud, faCloudUpload,
    faCodeFork,
    faCogs, faFileCode, faShare, faShoppingBasket,
    faTachometer,
    faTasks,
    faTerminal
} from "@fortawesome/free-solid-svg-icons";

export type SkillData = {
    name: string
    icon: IconDefinition
    tags: string[]
}

export const skills: Array<SkillData> = [
    {
        name: 'Agile Methods',
        icon: faTachometer,
        tags: [
            'Scrum',
            'Kanban',
            'Mixes'
        ]
    },
    {
        name: 'IDEs',
        icon: faTerminal,
        tags: [
            'IntelliJ',
            'Webstorm',
            'Eclipse'
        ]
    },
    {
        name: 'Frameworks',
        icon: faCogs,
        tags: [
            'Spring',
            'Vue / Nuxt',
            'React / Next'
        ]
    },
    {
        name: 'Testing',
        icon: faBugs,
        tags: [
            'J-Unit',
            'Selenium',
            'Cypress',
            'Playwright'
        ]
    },
    {
        name: 'Continous Int.',
        icon: faAlignJustify,
        tags: [
            'Jenkins',
            'CodePipeline',
            'gitLabCI',
        ]
    },
    {
        name: 'Versioning',
        icon: faCodeFork,
        tags: [
            'Git',
            'SVN'
        ]
    },
    {
        name: 'Project Management',
        icon: faTasks,
        tags: [
            'Jira',
            'Redmine',
            'Pivotal'
        ]
    },
    {
        name: 'Virtualization',
        icon: faCloud,
        tags: [
            'Docker',
            'Vagrant',
            'Ansible'
        ]
    },
    {
        name: 'Orchestration',
        icon: faCloudUpload,
        tags: [
            'Kubernetes',
            'Helm',
            'Cloudformation'
        ]
    },
    {
        name: 'Knowledge Transfer',
        icon: faShare,
        tags: [
            'Confluence',
            'Obsidian'
        ]
    },
    {
        name: 'E-Commerce Systems',
        icon: faShoppingBasket,
        tags: [
            'Hybris',
            'Konakart'
        ]
    },
    {
        name: 'CMS',
        icon: faFileCode,
        tags: [
            'Storyblok',
            'Bloomreach',
            'AEM'
        ]
    },
]
