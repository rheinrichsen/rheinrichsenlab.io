import React from 'react';

import { SkillData } from "../data/skills.ts";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "./entry.module.css";

interface EntryProps {
    skillData: SkillData
}

export const Entry: React.FC<EntryProps> = ({skillData}) => {
    return (
        <div className={styles.entry}>
            <FontAwesomeIcon className={styles.entryIcon} icon={skillData.icon}/>
            <div className={styles.contentBlock}>
                <div>
                    <span className={styles.contentTitle}>{skillData.name}:</span>
                </div>
                <div>
                    {skillData.tags.map((tag, index) => (
                        <span key={index} className={styles.contentPill}>{tag}</span>
                    ))}
                </div>

            </div>
        </div>
    )
}