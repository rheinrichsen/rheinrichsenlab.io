import React from 'react';

import {faGlobe} from "@fortawesome/free-solid-svg-icons";
import styles from './section.module.css'
import { languages } from "./data/languages.ts";
import { Entry } from "./entry/entry.tsx";
import {Title, TitleSize} from "../../common/title/title.tsx";

export const Section: React.FC = ({}) => {
    return (
        <div className={styles.list}>
            <Title title={"Languages"} icon={faGlobe} size={TitleSize.MEDIUM}/>
            {languages.map((language, index) => (
                <React.Fragment key={index}>
                    <Entry languageData={language}></Entry>
                </React.Fragment>
            ))}
        </div>
    )
}