import React from 'react';

import styles from "./entry.module.css";
import {LanguageData} from "../data/languages.ts";

interface EntryProps {
    languageData: LanguageData
}

export const Entry: React.FC<EntryProps> = ({languageData}) => {

    const languageWidth = {
        width: `${languageData.width}%`
    };

    return (
        <div className={styles.entry}>
            <div>{languageData.name}</div>
            <div className={styles.contentBarOuter} style={languageWidth}>
                <div className={styles.contentBarInner}>
                    {languageData.description}
                </div>
            </div>
        </div>
    )
}