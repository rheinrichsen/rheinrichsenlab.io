export type LanguageData = {
    name: string
    description: string
    width: number
}

export const languages: Array<LanguageData> = [
    {
        name: 'German',
        description: 'Mother tongue',
        width: 100
    },
    {
        name: 'English',
        description: 'Full working proficiency',
        width: 75
    }
]