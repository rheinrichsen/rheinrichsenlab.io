import { Section as PersonalInformationSection } from './personal-information/section.tsx'
import { Section as CodingLanguagesSection } from './coding-languages/section.tsx'
import { Section as SkillsSection } from './skills/section.tsx'
import { Section as LanguagesSection } from './languages/section.tsx'
import { Section as CertificatesSection } from './certificates/section.tsx'
import styles from './personal.module.css'
import {Divider, DividerSize} from "../common/divider/divider.tsx";
import {Photo} from "./photo/photo.tsx";

export const Personal: React.FC = ({}) => {
    return (
        <div className={styles.personal}>
            <Photo/>
            <div className={styles.personalInformation}>
                <PersonalInformationSection/>
                <Divider size={DividerSize.MEDIUM}/>
                <CodingLanguagesSection/>
                <Divider size={DividerSize.MEDIUM}/>
                <SkillsSection/>
                <Divider size={DividerSize.MEDIUM}/>
                <LanguagesSection/>
                <Divider size={DividerSize.MEDIUM}/>
                <CertificatesSection/>
            </div>
        </div>
    )
}
