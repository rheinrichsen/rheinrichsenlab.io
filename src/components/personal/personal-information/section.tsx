import React from 'react';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faBirthdayCake,
    faBriefcase,
    faEnvelope,
    faHome,
    faRightToBracket
} from "@fortawesome/free-solid-svg-icons";
import styles from './section.module.css'

export const Section: React.FC = ({}) => {
    return (
        <div className={styles.list}>
            <div className={styles.entry}>
                <FontAwesomeIcon size={"2x"} className={styles.icon} icon={faBriefcase}/>
                <span>Senior Software Engineer</span>
            </div>
            <div className={styles.entry}>
                <FontAwesomeIcon size={"2x"} className={styles.icon} icon={faHome}/>
                <span>Brandenburg, Germany</span>
            </div>
            <div className={styles.entry}>
                <FontAwesomeIcon size={"2x"} className={styles.icon} icon={faBirthdayCake}/>
                <span>07/1989</span>
            </div>
            <div className={styles.entry}>
                <FontAwesomeIcon size={"2x"} className={styles.icon} icon={faEnvelope}/>
                <span>
                    <a id={"e-mail-address"} className={styles.link} href="mailto:robert.heinrichsen.dev@gmail.com">Robert.Heinrichsen.Dev</a>
                </span>
            </div>
            <div className={styles.entry}>
                <FontAwesomeIcon size={"2x"} className={styles.icon} icon={faRightToBracket}/>
                <span>
                    <a id={"homepage"} className={styles.link} href="https://rh-coding-lab.netlify.app/" target="_blank">Coding-Lab</a>
                </span>
            </div>
        </div>
    )
}