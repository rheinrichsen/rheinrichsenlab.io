import React from 'react';
import styles from './photo.module.css'

export const Photo: React.FC = () => {
    return (
        <div className={styles.imageContainer}>
            <img src="/img/rheinrichsen.jpg" className={styles.image} alt="Avatar"/>
            <div className={styles.imageTextContainer}>
                <div className={styles.imageText}>Robert Heinrichsen</div>
            </div>
        </div>
    )
}