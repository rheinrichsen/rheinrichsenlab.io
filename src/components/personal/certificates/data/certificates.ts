export type CertificateData = {
    name: string
    from: string
    to?: string
}

export const certificates: Array<CertificateData> = [
    {
        name: 'Scrum Master',
        from: '2016',
        to: '2023'
    },
    {
        name: 'Scrum Developer',
        from: '2016',
        to: '2023'
    },
    {
        name: 'Bloomreach Developer',
        from: '2014',
        to: '2023'
    },
    {
        name: 'AWS Practitioner',
        from: '2021',
        to: '2023'
    }
]