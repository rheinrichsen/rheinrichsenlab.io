import React from 'react';

import styles from "./entry.module.css";
import {CertificateData} from "../data/certificates.ts";

interface EntryProps {
    certificateData: CertificateData
}

export const Entry: React.FC<EntryProps> = ({certificateData}) => {

    return (
        <div className={styles.entry}>
            <div>{certificateData.name}</div>
            <div className={styles.contentBarOuter}>
                <div className={styles.contentBarInner}>
                    {certificateData.from} - {certificateData.to}
                </div>
            </div>
        </div>
    )
}