import React from 'react';

import { faCertificate } from "@fortawesome/free-solid-svg-icons";
import styles from './section.module.css'
import { certificates } from "./data/certificates.ts";
import { Entry } from "./entry/entry.tsx";
import {Title, TitleSize} from "../../common/title/title.tsx";

export const Section: React.FC = ({}) => {
    return (
        <div className={styles.list}>
            <Title title={"Certificates"} icon={faCertificate} size={TitleSize.MEDIUM}/>
            {certificates.map((certificate, index) => (
                <React.Fragment key={index}>
                    <Entry certificateData={certificate}/>
                </React.Fragment>
            ))}
        </div>
    )
}