import React from 'react';

import { education } from "./data/education.ts";
import { EducationEntry } from "./education-entry.tsx";
import { faBook } from "@fortawesome/free-solid-svg-icons";
import styles from './education-list.module.css'
import {Title} from "../common/title/title.tsx";
import {Divider} from "../common/divider/divider.tsx";

export const EducationList: React.FC = ({}) => {
    return (
        <div className={styles.educationList}>
            <Title title={"Education"} icon={faBook}/>
            {education.map((entry, index) => (
                <React.Fragment key={index}>
                    <EducationEntry educationData={entry}/>
                    {index !== education.length - 1 && <Divider/>}
                </React.Fragment>
            ))}
        </div>
    )
}
