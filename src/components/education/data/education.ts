export type EducationData = {
    schoolName: string
    from: string
    to: string
    grade: string
}

export const education: Array<EducationData> = [
    {
        schoolName: 'Beuth Hochschule für Technik Berlin',
        from: '02/2011',
        to: '07/2014',
        grade: 'Bachelor of Science in Software Engineering'
    },
    {
        schoolName: 'KGS Salzhemmendorf',
        from: '08/2007',
        to: '06/2010',
        grade: 'Abitur (Higher education entrance qualification)'
    }
]
