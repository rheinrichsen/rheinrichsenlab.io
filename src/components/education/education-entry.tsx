import React from 'react';

import { EducationData } from "./data/education.ts";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendar } from "@fortawesome/free-solid-svg-icons";
import styles from './education-entry.module.css'

interface EducationProps {
    educationData: EducationData
}

export const EducationEntry: React.FC<EducationProps> = ({educationData}) => {
    return (
        <div className={styles.entry}>
            <span className={styles.name}>{educationData.schoolName}</span>
            <div className={styles.date}>
                <FontAwesomeIcon className={styles.dateIcon} icon={faCalendar}/>
                <span>{educationData.from} - {educationData.to}</span>
            </div>
            <div>{educationData.grade}</div>
        </div>
    )
}