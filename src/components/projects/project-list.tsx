import React from 'react';
import styles from './project-list.module.css'
import { ProjectEntry } from "./project-entry.tsx";
import { projects } from './data/projects.ts'
import { faTelevision } from "@fortawesome/free-solid-svg-icons";
import {Title} from "../common/title/title.tsx";
import {Divider} from "../common/divider/divider.tsx";

export const ProjectList: React.FC = ({}) => {
    return (
        <div className={styles.projectList}>
            <Title title={"Project Experience"} icon={faTelevision}/>
            {projects.map((project, index) => (
                <React.Fragment key={index}>
                    <ProjectEntry projectData={project}/>
                    {index !== projects.length - 1 && <Divider/>}
                </React.Fragment>
            ))}
        </div>
    )
}
