export type ProjectData = {
    title: string
    company: string
    role: string
    tasks: string[];
    projectOrganization: string[];
    systemTechnologies: string[];
}

export const projects: Array<ProjectData> = [
    {
        title: 'Create a web application',
        company: 'Engel & Völkers Technology GmbH',
        role: 'Senior Software Engineer',
        tasks: [
            'Support creating and analyzing requirements',
            'Implementing requirements',
            'Fullstack'
        ],
        projectOrganization: ['Agile'],
        systemTechnologies: ['node.js, typescript', 'React, Keycloak', 'Google Cloud']
    },
    {
        title: 'Create a customer portal',
        company: 'German vacuum products manufacturer',
        role: 'CMS Expert',
        tasks: [
            'Do requirements-engineering from a design concept',
            'Develop a CMS content architecture',
            'Create a technical solution for the Multi-region-multi-language concept',
            'Be a connection between frontend devs and backend devs',
            'Be a part of both development teams'
        ],
        projectOrganization: ['Scrum'],
        systemTechnologies: ['Hybris, Keycloak', 'Nuxt, Storyblok', 'Azure']
    },
    {
        title: 'Create a web-portal as foundation for the startup',
        company: 'BIM people connector startup',
        role: 'System Architect',
        tasks: [
            'Develop a system architecture for the customer',
            'Be a part of the development team'
        ],
        projectOrganization: ['Scrum'],
        systemTechnologies: ['AWS: Cognito, DynamoDB, S3, Cloudfront', 'Nuxt, Storyblok']
    },
    {
        title: 'CMS switch with frontend rework',
        company: 'German bank',
        role: 'CMS Developer',
        tasks: [
            'Be a part of the development team',
            'Rework web components'
        ],
        projectOrganization: ['Agile'],
        systemTechnologies: ['Adobe Experience Manager', 'Java / Javascript']
    },
    {
        title: 'Support developing a webshop',
        company: 'German shopping club',
        role: 'Senior Software Developer',
        tasks: [
            'Customization and extension of the webshop / Implementing functional and non functional requirements',
            'Analyze new technologies for usage in the system'
        ],
        projectOrganization: ['Agile'],
        systemTechnologies: ['Spring Application - Spring Boot, Spring MVC', 'mySQL - Percona', 'Connection to different interfaces - SalesForce, Ebay-Product-API']
    },
    {
        title: 'Develop a multi-webshop-system',
        company: 'German groceries wholesale business association',
        role: 'Senior Software Developer',
        tasks: [
            'Implementing Hippo CMS - Konakart integration',
            'Customization and Extension of the shop solution',
            'Analyze and improve the shop solution'
        ],
        projectOrganization: ['Scrum'],
        systemTechnologies: ['Hippo CMS (Bloomreach experience) - Konakart integration', 'Spring application with Hippo CMS and the shop-solution', 'AWS services with Spring - Bloomreach implementation (Custom development)']
    },
    {
        title: 'Implement a marketing platform',
        company: 'German bank association',
        role: 'CMS Architect',
        tasks: [
            'Create architecture for a modern web-application with CMS integration',
            'Consult in ux and ui for the platform',
            'Implement requirements'
        ],
        projectOrganization: ['Scrum'],
        systemTechnologies: ['Content: Bloomreach experience', 'Middleware: Spring application with Kotlin', 'Frontend: React', 'Communication between layers: GraphQL and Restful interfaces']
    },
    {
        title: 'Support in different internal projects',
        company: 'German press agency',
        role: 'Senior Software Developer',
        tasks: [
            'Transformation of datastructures for internal processes',
            'Evaluation of different cloud providers to support editor-teams',
            'Creation of an analyze tool to support editor-teams'
        ],
        projectOrganization: ['Kanban'],
        systemTechnologies: ['Mule ESB', 'Angular Applications', 'SAP integrations']
    },
    {
        title: 'Proof of concept for a CMS - E-Commerce integration',
        company: 'Switzerland energy company',
        role: 'Software Developer',
        tasks: ['Analyze architecture for a Konakart integration into a custom development application'],
        projectOrganization: [],
        systemTechnologies: []
    },
    {
        title: 'Maintenance and extension of a Web-Application',
        company: 'Swiss natural cosmetics manufacturer',
        role: 'Software Developer / CMS and E-Commerce Consultant',
        tasks: [
            'Implementing new requirements for CMS',
            'Concepting and implementing responsive design for the web-application',
            'Bugfixing for the webshop rollout'
        ],
        projectOrganization: ['Agile'],
        systemTechnologies: ['Hippo CMS (Bloomreach experience) - Konakart integration']
    }
]
