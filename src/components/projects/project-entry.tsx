import React, {useState} from 'react';
import styles from './project-entry.module.css'
import { ProjectData } from "./data/projects.ts";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";

// TODO Better design the informations

interface ProjectProps {
    projectData: ProjectData
}

export const ProjectEntry: React.FC<ProjectProps> = ({projectData}) => {
    const [isActive, setIsActive] = useState(false);

    const toggleActive = () => {
        setIsActive(!isActive);
    };

    return (
        <div className={styles.projectInfo}>
            <div className={styles.titleContainer}>
                <span className={styles.titleLabel}>{projectData.title}</span> / <span className={styles.titleCompany}>{projectData.company}</span>
            </div>
            <div className={styles.roleContainer}>
                <FontAwesomeIcon className={styles.roleIcon} icon={faUser} />
                <span>{projectData.role}</span>
            </div>
            <button
                className={`${styles.accordion} project-button`}
                onClick={toggleActive}
            >
                {isActive ? '- Hide' : '+ More Information'}
            </button>
            {isActive && (
                <div className={styles.projectContent}>
                    <div className={styles.contentEntry}>
                        <span className={styles.contentHeadline}>Tasks:</span>
                        <ul>
                            {projectData.tasks.map((task, index) => (
                                <li key={index} className={styles.contentListEntry}>{task}</li>
                            ))}
                        </ul>
                    </div>
                    <div className={styles.contentEntry}>
                        <span className={styles.contentHeadline}>Project Organization:</span>
                        <ul>
                            {projectData.projectOrganization.map((org, index) => (
                                <li key={index} className={styles.contentListEntry}>{org}</li>
                            ))}
                        </ul>
                    </div>
                    <div className={styles.contentEntry}>
                        <span className={styles.contentHeadline}>System-Technologies:</span>
                        <ul>
                            {projectData.systemTechnologies.map((tech, index) => (
                                <li key={index} className={styles.contentListEntry}>{tech}</li>
                            ))}
                        </ul>
                    </div>
                </div>
            )}
        </div>
    )
}