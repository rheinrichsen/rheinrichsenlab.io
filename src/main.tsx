import { StrictMode } from 'react'
import ReactDOM from 'react-dom/client'
import {Cv} from "./cv.tsx";
import './reset.css'
import './main.css'


// Render the app
const rootElement = document.getElementById('app')!
if (!rootElement.innerHTML) {
    const root = ReactDOM.createRoot(rootElement)
    root.render(
        <StrictMode>
            <Cv/>
        </StrictMode>,
    )
}